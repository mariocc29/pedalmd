# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require 'open-uri'
require 'csv'


def read(url)
    CSV.new(URI.open(url), :headers => :first_row).each do |line|
        Pokemon.create({
            name: line[0],
            type_1: line[1],
            type_2: line[2],
            hp: line[3],
            attack: line[4],
            defense: line[5],
            sp_atk: line[6],
            sp_def: line[7],
            speed: line[8],
            generation: line[9],
            legendary: line[10]
        })
    end
end

url = 'https://gist.githubusercontent.com/armgilles/194bcff35001e7eb53a2a8b441e8b2c6/raw/92200bc0a673d5ce2110aaad4544ed6c4010f687/pokemon.csv'
read(url)
