class PokemonsController < ApplicationController

  before_action :record, only: [:show, :update, :destroy]

  # GET /pokemons
  def index
    @pokemons = paginate Pokemon.all
    
    json_response({
      pagination:
        {
          total_records: Pokemon.all.count,
          limit_value: @pokemons.limit_value, 
          total_pages: @pokemons.total_pages, 
          current_page: @pokemons.current_page, 
          next_page: @pokemons.next_page, 
          prev_page: @pokemons.prev_page, 
          first_page: @pokemons.first_page?,
          last_page: @pokemons.last_page?,
          out_of_range: @pokemons.out_of_range?
        },
      data: @pokemons
    })
  end

  # POST /pokemons
  def create
    pokemon = Pokemon.create!(keys_allowed)
    json_response(pokemon, :ok)
  end

  # GET /pokemons/:id
  def show
    json_response(@pokemon)
  end

  # PUT /pokemons/:id
  def update
    @pokemon.update(keys_allowed)
    head :no_content
  end

  # DELETE /pokemons/:id
  def destroy
    @pokemon.destroy
    head :no_content
  end

  private

  attr_accessor :pokemon

  # Keys allowed in the request
  def keys_allowed
    validate(RequestValidator.new, JSON.parse(request.body.read))
  end

  def record
    @pokemon = Pokemon.find(params[:id])
  end
end
