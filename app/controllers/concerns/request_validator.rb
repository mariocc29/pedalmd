require 'dry-validation'

class RequestValidator < Dry::Validation::Contract
    params do
        required(:name).filled(:string)
        required(:type_1).filled(:string)
        required(:type_2).filled(:string)
        required(:hp).filled(:integer)
        required(:attack).filled(:integer)
        required(:defense).filled(:integer)
        required(:sp_atk).filled(:integer)
        required(:sp_def).filled(:integer)
        required(:speed).filled(:integer)
        required(:generation).filled(:integer)
        optional(:legendary).maybe(:bool)
    end
end