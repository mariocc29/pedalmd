class ApplicationController < ActionController::API
    include ApiResponse
    include ExceptionHandler
    include ValidatorHelper
    include Rails::Pagination
end
