module ValidatorHelper
  # Helper to render the validators
  def validate(contract, request)
    result = contract.call(request)

    if result.failure?
      errors = []
      result.errors.to_h.each do |(attribute, error)|
        errors.push("#{attribute} #{error.first}")
      end

      raise Exception.new errors.join(' | ')
    else
      result.to_h
    end
  end
end
