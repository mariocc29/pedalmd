FROM ruby:2.7

RUN apt-get update \
    && apt-get install -y sqlite3 libsqlite3-dev \
    && apt-get install -y --no-install-recommends \
    && rm -rf /var/lib/apt/lists/*

# Install NodeJS
RUN curl --silent --show-error --location https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add - \
    && echo "deb https://deb.nodesource.com/node_6.x/ stretch main" > /etc/apt/sources.list.d/nodesource.list \
    && apt-get update \
    && apt-get install -y nodejs \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Install Yarn
RUN curl --silent --show-error --location https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
    && echo "deb https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list \
    && apt-get update \
    && apt-get install -y yarn \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

WORKDIR /app
COPY . /app

ENV TZ="America/Montreal"
RUN date

RUN cd /app
RUN gem install bundler
RUN gem install sqlite3
RUN bundle install

EXPOSE 3000