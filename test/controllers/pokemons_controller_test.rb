require 'faker'

class PokemonsControllerTest < ActionDispatch::IntegrationTest
  include FactoryBot::Syntax::Methods

  setup do
    @pokemon = Pokemon::first[:id]
  end

  test "should get index" do
    get pokemons_url, as: :json
    assert_response :success
  end

  test "should create pokemon" do
    post pokemons_url, params: build(:pokemon), as: :json
    assert_response :success
  end

  test "should show pokemon" do
    get pokemon_url(@pokemon), as: :json
    assert_response :success
  end

  test "should update pokemon" do
    put pokemon_url(@pokemon), params: build(:pokemon, name: Faker::Games::Pokemon.name), as: :json
    assert_response :success
  end

  test "should destroy pokemon" do
    assert_difference('Pokemon.count', -1) do
      delete pokemon_url(@pokemon), as: :json
    end

    assert_response 204
  end
end
