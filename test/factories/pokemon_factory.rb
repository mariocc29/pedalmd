FactoryBot.define do
  factory :pokemon do
    name { "Bulbasaur" }
    type_1 { "Grass" }
    type_2 { "Poison" }
    hp { 45 }
    attack { 49 }
    defense { 49 }
    sp_atk { 65 }
    sp_def { 65 }
    speed { 45 }
    generation { 1 }
    legendary { false }
  end
end