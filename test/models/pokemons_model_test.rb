class PokemonTest < ActiveSupport::TestCase
    include FactoryBot::Syntax::Methods

    test "complete " do
        record = build(:pokemon)
        assert record.valid?
    end

    test "missing name " do
        record = build(:pokemon, name: nil)
        assert_not record.valid?
    end

    test "missing type_1 " do
        record = build(:pokemon, type_1: nil)
        assert_not record.valid?
    end

    test "missing type_2 " do
        record = build(:pokemon, type_2: nil)
        assert_not record.valid?
    end

    test "missing hp " do
        record = build(:pokemon, hp: nil)
        assert_not record.valid?
    end

    test "missing attack " do
        record = build(:pokemon, attack: nil)
        assert_not record.valid?
    end

    test "missing defense " do
        record = build(:pokemon, defense: nil)
        assert_not record.valid?
    end

    test "missing sp_atk " do
        record = build(:pokemon, sp_atk: nil)
        assert_not record.valid?
    end

    test "missing sp_def " do
        record = build(:pokemon, sp_def: nil)
        assert_not record.valid?
    end

    test "missing speed " do
        record = build(:pokemon, speed: nil)
        assert_not record.valid?
    end

    test "missing generation " do
        record = build(:pokemon, generation: nil)
        assert_not record.valid?
    end

    test "missing legendary " do
        record = build(:pokemon, legendary: nil)
        assert record.valid?
    end
end