# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

ruby '2.7.4'

* System dependencies

* Configuration

* Database creation

- rails db:migrate
- rails db:rollback [In case to do it]


* Database initialization

rails db:seed

* How to run the test suite

rails test test/models/pokemons_model_test.rb
rails test test/controllers/pokemons_controller_test.rb

* Routes:

curl --location --request GET 'localhost:3000/pokemons'

curl --location --request POST 'localhost:3000/pokemons' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "Bulbasaur",
    "type_1": "Grass",
    "type_2": "Poison",
    "total": 318,
    "hp": 45,
    "attack": 49,
    "defense": 49,
    "sp_atk": 65,
    "sp_def": 65,
    "speed": 45,
    "generation": 1,
    "legendary": false
}'

curl --location --request GET 'localhost:3000/pokemons/:id'

curl --location --request PUT 'localhost:3000/pokemons/:id' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "Bulbasaur",
    "type_1": "Grass",
    "type_2": "Poison",
    "total": 318,
    "hp": 45,
    "attack": 49,
    "defense": 49,
    "sp_atk": 65,
    "sp_def": 65,
    "speed": 45,
    "generation": 1,
    "legendary": false
}'

curl --location --request DELETE 'localhost:3000/pokemons/:id'


* Deployment instructions

docker-compose up -d --build api